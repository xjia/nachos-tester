nachos-tester - Tester for Nachos course project
=============

Troubleshooting
---------------

- timeout command not found: It is very likely that you are running Mac OS.  Please install Homebrew (if you haven't) and run `brew install coreutils`.  After installation, start a new shell window and try again.  This shouldn't happen under Linux (e.g. Ubuntu).
- git operator always times out: Make sure you have cloned something from Bitbucket before, so that Bitbucket is added to your known hosts list.  To be specific, you should be asked the question "Are you sure you want to continue connecting (yes/no)?" (Google this question if you don't know what is it).
