#!/usr/bin/env python
import argparse
def main():
    parser = argparse.ArgumentParser(description='log to csv for nachos')
    parser.add_argument('--log', dest='log', action='store', metavar='LOG_FILE', required=True,
                        help='Filename of log file to analyze')
    #parser.add_argument('--sample', dest='sample', action='store', metavar='SAMPLE_FILE', required=True,
    #                    help='Filename of sample log file')
    parser.add_argument('--csv',dest='csv',action='store', metavar='CSV_FILE', required=True,
                        help='Filename of output .csv file')
    args = parser.parse_args()
    filelog = open(args.log, 'r')
    #filesample = open(args.sample, 'r')
    filecsv = open(args.csv, 'w')
    log = []
    while True:
        line = filelog.readline()
        if(not line):
            break
        log.append(line)
    #sample = []
    #while True:
    #    line = filesample.readline()
    #    if(not line):
    #        break
    #    sample.append(line)
    testcase = {}
    result = {}
    for i in range(len(log)):
        if(log[i].startswith('nachos-sjtu initializing...')):
            if(log[i-1].startswith('*** TEST')):
                testcase[log[i-1]] = None
    for i in range(len(log)):
        if(log[i].startswith('(Tag p')):
            studentid = None
            length = len('Student directory: student_repos/5110309061\n')
            if(i+2 < len(log) and len(log[i+2]) == length):
                studentid = log[i+2][length-11: length-1]+'('+log[i][5:len(log[i])-1]
            if(studentid == None):
                continue
            else:
                result[studentid] = {}
            #print studentid
            for j in range(i+1, len(log)):
                if(log[j].startswith('(Tag p')):
                    break
                if(log[j] in testcase):
                    for k in range(j+1, len(log)):
                        if(log[j].startswith('(Tag p') or (log[k] in testcase)):
                            result[studentid][log[j]] = 'timeout'
                            break
                        if(log[k].find(' real')>=0 and
                           log[k].find(' user')>=0 and
                           log[k].find(' sys')>=0):
                            timeout = False
                            if(log[k-1][0:len(log[k-1])-1] == 'success'):
                                result[studentid][log[j]] = 'success'
                            else:
                                result[studentid][log[j]] = 'fail'
                            #result[studentid][log[j]] = log[k-1][0:len(log[k-1])-1]
                            break
                    pass #for k
                pass #for j
        pass #for i
    for case in testcase:
        filecsv.write('\t'+case[9:len(case)-2])
    filecsv.write('\n')
    for studentid in result:
        filecsv.write(studentid)
        for case in testcase:
            if(case in result[studentid]):
                #filecsv.write(';'+'\"'+result[studentid][case]+'\"')
                filecsv.write('\t'+result[studentid][case])
            else:
                filecsv.write('\t?')
        filecsv.write('\n')
    filelog.close()
    #filesample.close()
    filecsv.close()


if __name__ == '__main__':
    main()
